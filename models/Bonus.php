<?php

namespace app\models;

use Yii;
	 

/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $reasonid
 * @property integer $amount
 *
 * @property Lead $user
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'userid', 'reasonid', 'amount'], 'required'],
            [['id', 'userid', /*'amount'*/], 'integer'],  //////////////////// 3c
				array('amount', 'integer',
				'integerOnly'=>true,
				'min'=>100,
				'max'=>1000,
				'tooSmall'=>'Bonus must be above 100 Shekels',
				'tooBig'=>'Bonus must be less than 1000 Shekels'),
			/////////////////////////////////////////////////////////
            [['reasonid'], 'string', 'max' => 55],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'reasonid' => 'Reasonid',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Lead::className(), ['id' => 'userid']);
    }
}
