<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper; ///////////////// 

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $id
 * @property string $name
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 55],
        ];
    }
////////////////////////////////////////////////////////////// 3a
			public static function getReasonid()
	{
		$allLeadid = self::find()->all();
		$allLeadidArray = ArrayHelper::
					map($allLeadid, 'id', 'name');
		return $allLeadidArray;						
	}
	
/////////////////////////////////////////////////////	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
